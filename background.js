console.log("backgroud running..");

var documentStart = false;
var listenLoading = true;
var listenComplete = true;
var loop;;

var StartTime;
var portalURL = 'http://localhost:8484'
var isValidUrl = '(https?:\/\/)?[\w\-~]+(\.[\w\-~]+)+(\/[\w\-~]*)*(#[\w\-]*)?(\?.*)?';

var urlList = [];
var uniqiueStingList = [];

function guid() {
  return s4() + s4() + s4();
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}

// ---- ON-EXTENSION INSTALLED  ----
// Call server and add to database.
chrome.runtime.onInstalled.addListener(function (details) {
  chrome.storage.local.set({
    switch: "_OFF_"
  });
  chrome.system.cpu.getInfo(function (info) {
    chrome.system.memory.getInfo(function (memory) {
      if (details.reason == "install") {
        var now = new Date();
        $.ajax({
          url: `${portalURL}/api/addClientInstance`,
          data: {
            runtime_id: chrome.runtime.id,
            cpu_numberOfCore: info.numOfProcessors,
            cpu_archName: info.archName,
            cpu_modelName: info.modelName,
            ram_size: memory.capacity,
            date_joined: new Date(now).toISOString()
          },
          dataType: "json",
          type: "post",
          success: function (data) {
            if (data.message == "SUCCESS") {
              _clientId = data.clientId;
              chrome.storage.local.set({
                clientId: _clientId
              }, function () {
                chrome.runtime.setUninstallURL(
                  `${portalURL}/api/updateClientStopStauts/${_clientId}`,
                  function () {
                    console.log("Uninstall URL is set with clientID: " + _clientId);
                  }
                );
              });
            } else {
              alert("Problem Connecting to QoS Portal Server");
            }
          },
          error: function (e) {
            alert("Problem Connecting to QoS Portal Server");
          }
        });
      }
    });
  });
});
// --- ON-CHANGES MADE TO THE CHROME LOCAL STORAGE ----
// When monitoring switch is turned on/off
chrome.storage.onChanged.addListener(function (changes, namespace) {
  if (changes.switch) {
    if (changes.switch.newValue === "_ON_") {
      setBadgeAndNotificaion();
      startMonitoringSession();
    } else if (changes.switch.newValue === "_OFF_") {
      stopMonitoringSession();
      chrome.browserAction.setBadgeText({
        text: ""
      });
    }
  }
});
// -- ON-URL LOADED/RELOADED ON ANY TAB ON THE BROWSER --
// chrome.tabs.onUpdated.addListener(function (q, w, e) {
 
//   var now = new Date();

//   chrome.storage.local.get("switch", function (data) {
//     var _switch = data.switch;
   
//        if(navigator.onLine){
//     if (_switch == "_ON_") {
      
//       chrome.storage.local.get("operationId", function (data) {
//         var _operationId = data.operationId;

//         var mainUrl = e.url.split('//')[1].split('/')[0];
//         if (urlList.includes(mainUrl)) {
//           var indexO = urlList.indexOf(mainUrl);
//           if (e.url.includes(uniqiueStingList[indexO])) {
//             // console.log(e);
//             if (e.status === 'loading' && listenLoading) {
//               StartTime = Date.now();
//               console.log("Opening Loading:", StartTime)
//               listenLoading = false
//             }

//             if (e.status === 'complete' && listenComplete && !listenLoading) {
//               var StopTime = Date.now();
//               console.log("Opening Compete:", StopTime);
//               var totalTime = (StopTime - StartTime) / 1000;
//               listenComplete = false;
//               // console.log(totalTime);
//               var notifOptions = {
//                 type: "basic",
//                 iconUrl: "static/icon.png",
//                 title: "Cloud QoS Monitor: "+mainUrl,
//                 message: totalTime + " seconds"
//               };
//               chrome.notifications.create((Math.floor(Math.random() * 90000) + 10000).toString(), notifOptions, () => {
//                 setTimeout(function () {
//                   var notifOptions2 = {
//                     type: "basic",
//                     iconUrl: "static/icon.png",

//                     title: "Cloud QoS Monitor: Processing",
//                     message: "System Restart Monitoring, Please wait"
//                   };
//                   chrome.notifications.create((Math.floor(Math.random() * 90000) + 10000).toString(), notifOptions2, () => {
//                     setTimeout(function () {
//                       // Save Monitored site in the web api using AJAX call
//                       $.ajax({
//                         url: `${portalURL}/api/addSiteMonitored`,
//                         data: {
//                           operation_id: _operationId,
//                           name: mainUrl.split('.')[1].toString(),
//                           mainUrl: mainUrl,
//                           partialUrl: uniqiueStingList[indexO],
//                           loadtime: totalTime,
//                           net_rtt: window.navigator.connection.rtt,
//                           net_downlink: window.navigator.connection.downlink,
//                           net_effectiveType: window.navigator.connection.effectiveType
//                         },
//                         dataType: "json",
//                         type: "post",
//                         success: function (data) {
//                           //On request successful check server responce: SUCCESS/ERROR
//                           if (data.message == "SUCCESS") {

//                             // Store created operation id to chrome local storage
//                             var notifOptions3 = {
//                               type: "basic",
//                               iconUrl: "static/icon.png",

//                               title: "Cloud QoS Monitor: Processing",
//                               message: "Complete: Monitoring Disabled"
//                             };
//                             chrome.notifications.create((Math.floor(Math.random() * 90000) + 10000).toString(), notifOptions3, () => {

//                               listenComplete = true;
//                               listenLoading = true;
//                             });
//                             chrome.storage.local.set({
//                               switch: "_OFF_"
//                             });
//                           } else {

//                       chrome.storage.local.set({ switch: "_OFF_" });
//                             alert("Problem Connecting to QoS Server");
//                           }
//                         },
//                         error: function (e) {
//                           chrome.storage.local.set({ switch: "_OFF_" });
//                           alert("Problem Connecting to QoS Server");
//                         }
//                       });
//                     }, 7000);
//                   })
//                 }, 4000);
//               });

//             }
//           }
//         }
//       });
//     }
//   }else{
//     var notifOptions3 = {
//       type: "basic",
//       iconUrl: "static/icon.png",
  
//       title: "Network Error",
//       message: "Your Computter is offline"
//     };
//     chrome.notifications.create((Math.floor(Math.random() * 90000) + 10000).toString(), notifOptions3, () => {
//       chrome.storage.local.set({ switch: "_OFF_" });
  
//   });
// }
//   });

//   });

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    var mainUrl = sender.url.split('//')[1].split('/')[0];

    console.log("Data Sent",request);
    if (urlList.includes(mainUrl)) {
    var totalTime = (request.timing.duration/1000).toFixed(3);
    chrome.storage.local.get("switch", function (data) {
          var _switch = data.switch;
         if(_switch == "_ON_"){
             if(navigator.onLine){
            
    // console.log("Data Sent",request);
            chrome.storage.local.get("operationId", function (data) {
              var _operationId = data.operationId;
      
        $.ajax({
            url: `${portalURL}/api/addSiteMonitored`,
            data: {
              operation_id: _operationId,
              name: mainUrl.split('.')[1],
              mainUrl: mainUrl,
              partialUrl: sender.url.split(mainUrl)[1],
              loadtime: totalTime,
              net_rtt: window.navigator.connection.rtt,
              net_downlink: window.navigator.connection.downlink,
              net_effectiveType: window.navigator.connection.effectiveType
            },
            dataType: "json",
            type: "post",
            success: function (data) {
              var notifOptions = {
                type: "basic",
                iconUrl: "static/icon.png",
                title: "Cloud QoS Monitor: "+ mainUrl +"[Complete]",
                message: totalTime + " seconds"
              };
            chrome.notifications.create((Math.floor(Math.random() * 90000) + 10000).toString(), notifOptions, () => {

            });
            },
            error: function (e) {
              chrome.storage.local.set({ switch: "_OFF_" });
              alert("Problem Connecting to QoS Server");
            }
          });
        });
      } else{
            var notifOptions3 = {
              type: "basic",
              iconUrl: "static/icon.png",
          
              title: "Network Error",
              message: "Your Computter is offline"
            };
            chrome.notifications.create((Math.floor(Math.random() * 90000) + 10000).toString(), notifOptions3, () => {
              chrome.storage.local.set({ switch: "_OFF_" });
          
          });
        }
      }
    });
  }
    // This cache stores page load time for each tab, so they don't interfere
    // chrome.storage.local.get('cache', function(data) {
    //   if (!data.cache) data.cache = {};
    //   data.cache['tab' + sender.tab.id] = request.timing;
    //   chrome.storage.local.set(data);
    // });

  }
);

  setBadgeAndNotificaion = function () {
    chrome.browserAction.setBadgeText({
      text: "ON"
    });
    chrome.browserAction.setBadgeBackgroundColor({
      color: "#228B22"
    });
    var notifOptions = {
      type: "basic",
      iconUrl: "static/icon.png",
      title: "Cloud QoS Monitor",
      message: "Monitoring System has been turned on"
    };
    chrome.notifications.create((Math.floor(Math.random() * 90000) + 10000).toString(), notifOptions);
  };
  // START monitoring proccess
  startMonitoringSession = function () {
    // Get clint id from local storage
    chrome.storage.local.get("clientId", function (data) {
      var _clientId = data.clientId;

      // Create new operation id
      var _operationId = guid();
      var now = new Date();

      // Save monitoring session in the web api using AJAX call
      $.ajax({
        url: `${portalURL}/api/addMonitingLog`,
        data: {
          operation_id: _operationId,
          client_id: _clientId,
          start_timestamp: new Date(now).toISOString()
        },
        dataType: "json",
        type: "post",
        success: function (data) {
          //On request successful check server responce: SUCCESS/ERROR
          if (data.message == "SUCCESS") {
// console.log(data);
            // Store created operation id to chrome local storage
            chrome.storage.local.set({
              operationId: _operationId
            });
            urlList = [];
            for(var i = 0; i < data.urls.length; i++){
              urlList.push(data.urls[i].mainURL);
              uniqiueStingList.push(data.urls[i].partialURL);
            }
          } else if(data.message == "NOURL"){

            chrome.storage.local.set({ switch: "_OFF_" });
            alert("Please Add URL to Be Monitored");

          } else if(data.message == "ERROR"){

            chrome.storage.local.set({ switch: "_OFF_" });
            alert("Problem Connecting to QoS Server");
          }
        },
        error: function (e) {
          chrome.storage.local.set({ switch: "_OFF_" });
          
          alert("Problem Connecting to QoS Server");
        }
      });
    });
  };
  stopMonitoringSession = function () {
    chrome.storage.local.get("operationId", function (data) {
      var _operationId = data.operationId;
      var now = new Date();
      $.ajax({
        url: `${portalURL}/api/updateMonitorStopStauts/${_operationId}`,
        data: {
          stop_timestamp: new Date(now).toISOString()
        },
        type: "get",
        success: function (data) {
          chrome.storage.local.set({
            operationId: "NULL"
          });
        }
      });
    });
  };

  // logNetworkStatus = function () {
  //       chrome.storage.local.get("operationId", function (data) {
  //         var _operationId = data.operationId;
  //         var now = new Date();
  //         $.ajax({
  //           url: `${portalURL}/api/addNetworkStatus`,
  //           data: {
  //             status_id: guid(),
  //             operation_id: _operationId,
  //             timestamp: new Date(now).toISOString(),
  //             net_rtt: window.navigator.connection.rtt,
  //             net_downlink: window.navigator.connection.downlink,
  //             net_effectiveType: window.navigator.connection.effectiveType
  //           },
  //           dataType: "json",
  //           type: "post",
  //           success: function (data) { }
  //         });
  //       });
  // };